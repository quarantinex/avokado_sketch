from crud import app
from os import getenv

# port = int(getenv('PORT', 5000))
# if __name__ == "__main__":
#     app.run(debug = True, host='0.0.0.0', port=port, threaded=True)
cf_port = getenv("PORT")
if __name__ == '__main__':
    if cf_port is None:
        app.run(host='0.0.0.0', port=5000, debug=True, threaded=True)
    else:
        app.run(host='0.0.0.0', port=int(cf_port), debug=True, threaded=True)
