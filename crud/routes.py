from crud import app
from flask import Flask, redirect, url_for, render_template, request
from .classes.inference.Sampler import *
import os

import re
from io import BytesIO
import base64

model_json_file = '/bin/model_json.json'
model_weights_file = '/bin/weights.h5'
output_folder =  './generated_html'

@app.route('/send_img', methods=['POST'])
def send_img():
    if request.method == 'POST':
        print("send img")
        key, value = list(dict(request.form).items())[0]
        key = key.replace('"', '')
        # print(key)
        print(type(key))
        # print(request.form)

        # print(request.values['imgBase64'])
        # image_b64 = request.form['imgBase64']
        # ts = request.values['timestamp']
        image_data = re.sub('^data:image/.+;base64,', '', key)  # to remove data:image/png;base64
        image_data1 = re.sub('\s', '+', image_data)
        print(image_data1)
        img = Image.open(BytesIO(base64.b64decode(image_data1+'==')))
        # html_filename = "sketch2html_result_" + ts
        img.save('origin.png')


        # with open("imageToSave.png", "wb") as fh:
        #     fh.write(base64.decodebytes(image_data1+'=='))
        png_path = 'origin.png'
        #--converting start--

        print("--converting starts--")
    
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        sampler = Sampler(model_json_path=model_json_file,
                          model_weights_path = model_weights_file)
        sampler.convert_single_image(output_folder, png_path=png_path)

        # sketch to out.html
        # print("sketch to out.html")
        #--converting time end--
    return 'OK'